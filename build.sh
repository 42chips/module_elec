#Subject list to build
PDFS=*/subject/

#calculate padding
padding=0
for subject in $PDFS; do
	if [[ $padding < ${#subject} ]]; then
		padding=${#subject}
	fi;
done

status=0

# Build subjects
printf "\e[1;34m[\e[0;34mInfo    \e[1m]\e[0m Buidling PDFs\n"
for subject in $PDFS; do
	printf "\e[36;1m[\e[0;36mBuilding\e[1m]\e[0m %s%-*s " $(dirname $subject) $(( $padding - ${#subject} )) '';
	error=$((make -C $subject 1>/dev/null 1>&3 2>&3) 3>&1);
	if [[ $? == 0 ]]; then
		printf "\e[1;32m[\e[0;32mDone\e[1m]\e[0m\n"
	else
		printf "\e[1;31m[\e[0;31mFAIL\e[1m]\e[0m\n"
		printf "$error\n"
		status=2
	fi;
done
# Clean directories
printf "\e[1;34m[\e[0;34mInfo    \e[1m]\e[0m Cleaning directories\n"
for subject in $PDFS; do
	printf "\e[1;35m[\e[0;35mCleaning\e[1m]\e[0m %s%-*s " $(dirname $subject) $(( $padding - ${#subject} )) '';
	make -C $subject clean &>/dev/null;
	printf "\e[1;32m[\e[0;32mDone\e[1m]\e[0m\n"
done
# Copy pdfs to output directory
printf "\e[1;34m[\e[0;34mInfo    \e[1m]\e[0m Moving generated subjects to \e[33mout\e[1m/\e[0m"
mkdir -p out
cp */subject/*.pdf out/
printf " \e[1;32m[\e[0;32mDone\e[1m]\e[0m\n"

if [[ $status != 0 ]]; then
	printf "\e[1;31m[\e[0;31mError   \e[1m]\e[0m Check the log above for more info.\n"
fi

exit $status
