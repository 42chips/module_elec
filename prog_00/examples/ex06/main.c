#include <avr/io.h>

void ft_delay_ms(unsigned int n){ // don't work with -Os
	unsigned int x;

	while(n--){
		x=2300;
		while(x--);
	}
}

int	main(void) {
	char led_mode = 0;

	DDRB |= (1 << PIN0) | (1 << PIN1) | (1 << PIN2) | (1 << PIN3); // OUTPUT
	// PORTB |= (0x0f); // test LEDs

	DDRD &= ~(1 << PIN2); // INPUT
	// PORTD |= (1 << PIN3); // Pull-up

	uint8_t i = 1;

	for(;;) {
		if (!(PIND & (1 << PIN2)) && !led_mode) {
			// PORTB ^= (1 << PIN3);
			PORTB = (PORTB & 0xf0) | (i++ & 0x0f);
			led_mode = 1;
			ft_delay_ms(30);
		}
		else if (PIND & (1 << PIN2) && led_mode) {
			led_mode = 0;
			ft_delay_ms(30);
		}
	}
}
