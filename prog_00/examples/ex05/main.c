#include <avr/io.h>

void ft_delay_ms(unsigned int n){ // don't work with -Os
	unsigned int x;

	while(n--){
		x=2300;
		while(x--);
	}
}

int	main(void) {
	uint8_t led_mode = 0;

	DDRB  |=  (1 << PORTB3); // OUTPUT
	DDRD  &= ~(1 << PORTD3); // INTPUT
	PORTD |=  (1 << PORTD3); // Pull-up

	for(;;) {
		if (!(PIND & (1 << PORTD3)) && !led_mode) {
			PORTB ^= (1 << PORTB3);
			led_mode = 1;
			ft_delay_ms(100);
		}
		else if (PIND & (1 << PORTD3) && led_mode) {
			led_mode = 0;
			ft_delay_ms(100);
		}
	}
}
