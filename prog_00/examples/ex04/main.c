#include <avr/io.h>

int	main(void) {
	DDRB |=  (1 << PORTB3); // OUTPUT
	DDRD &= ~(1 << PORTD2); // INTPUT

	// PORTD |= (1 << PORTD3); // Pull-up

	for(;;) {
		if (PIND & (1 << PORTD2))
			PORTB &= ~(1 << PORTB3);
		else
			PORTB |= (1 << PORTB3);
	}
}
