#include <avr/io.h>

#define UART_BAUDRATE 115200
#define MAX_INPUT 30

// #define BAUD_PRESCALE ((F_CPU / 16 / UART_BAUDRATE) - 1) // Async normal     20.3.1
#define BAUD_PRESCALE ((F_CPU / 8 / UART_BAUDRATE) - 1) // Async double speed   20.3.1

void ft_delay_ms(unsigned int n){ // don't work with -Os
	unsigned int x;

	while (n--){
		x = 2300;
		while (x--);
	}
}

void uart_init(void){
	UBRR0L = BAUD_PRESCALE;                 // Set baud rate                     20.11.5 UBRRnL
	UBRR0H = BAUD_PRESCALE >> 8;

	UCSR0A = 1 << U2X0;                     // Double speed                      20.11.2 UCSRnA
	UCSR0B = (1 << RXEN0) | (1 << TXEN0);   // Enable receiver and transmitter   20.11.3 UCSRnB
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); // 8-N-1                             20.11.4 UCSRnC
}

void uart_tx(unsigned char data){
	while(!(UCSR0A & (1 << UDRE0)));        // Wait for empty transmit buffer    20.11.2 UCSRnA
	UDR0 = data;                            //Put data into buffer and sends     20.11.1 UDRn
}

uint8_t uart_rx(void){
	while (!(UCSR0A & (1 << RXC0)));        // Wait for data to be received      20.11.2 UCSRnA
	return UDR0;                            // Get data from buffer              20.11.1 UDRn
}

void uart_printstr(const char *str){
	while (*str)
		uart_tx(*str++);
}

int8_t ft_strcmp(const char *s1, const char *s2){
	while (*s1 != '\0' && (*s1++ == *s2++));
	return (*(unsigned char *)--s1 - *(unsigned char *)--s2);
}

void getInput(char *str, uint8_t hide){
	char c;
	char *ptr = str;

	for (uint8_t i = 0; i < MAX_INPUT; i++) // clear input
		str[i] = 0;

	for (;;) {
		c = uart_rx();
		switch (c) {
		case 0x7F: // del
			if (ptr > str){
				*ptr-- = 0;
				uart_printstr("\b \b"); // remove char from screen
			}
			break;
		case '\r': // enter
			return;
		default:
			if (ptr < str + MAX_INPUT){
				*ptr++ = c;
				uart_tx(hide ? '*' : c);
			}
		}
	}
}

const char user[] = "spectre";
const char pass[] = "secret";

int main(void){
	char input[MAX_INPUT];

	uart_init();
	DDRB |= (1 << PIN3); // OUTPUT
	for (;;){
		uart_printstr("\r\nHello please login:\r\n");
		uart_printstr("\tUser: ");

		getInput(input, 0);
		if (!ft_strcmp(user, input)){
			uart_printstr("\r\n\tPass: ");
			getInput(input, 1);
			if (!ft_strcmp(pass, input)){
				uart_printstr("\r\nWelcome ");
				uart_printstr(user);
				for (;;){
					PORTB ^= (1 << PIN3);
					ft_delay_ms(100);
				}
			}
			else
				uart_printstr("\r\nWrong password\r\n");
		}
		else
			uart_printstr("\r\nNo user found\r\n");
	}
}
