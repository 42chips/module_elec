#include <avr/io.h>

#define UART_BAUDRATE 115200

// #define BAUD_PRESCALE ((F_CPU / 16 / UART_BAUDRATE) - 1) // Async normal     20.3.1
#define BAUD_PRESCALE ((F_CPU / 8 / UART_BAUDRATE) - 1) // Async double speed   20.3.1

void uart_init(void) {
	UBRR0L = BAUD_PRESCALE;                 // Set baud rate                     20.11.5 UBRRnL
	UBRR0H = BAUD_PRESCALE >> 8;

	UCSR0A = (1 << U2X0);					// Double speed                      20.11.2 UCSRnA
	UCSR0B = (1 << RXEN0)  | (1 << TXEN0);  // Enable receiver and transmitter   20.11.3 UCSRnB
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); // 8-N-1                             20.11.4 UCSRnC
}

void uart_tx(unsigned char data) {
	while (!(UCSR0A & (1 << UDRE0)));		 // Wait for empty transmit buffer   20.11.2 UCSRnA
	UDR0 = data;                             //Put data into buffer and sends    20.11.1 UDRn
}

uint8_t uart_rx(void) {
	while (!(UCSR0A & (1 << RXC0)));         // Wait for data to be received     20.11.2 UCSRnA
	return UDR0;                             // Get data from buffer             20.11.1 UDRn
}

void uart_printstr(const char *str) {
	while (*str)
		uart_tx(*str++);
}

const char str[] = "Hello World!\r\n";

int main(void) {
	uart_init();

	TCCR1A  = 0;                         // Normal port operation, OC1A/OC1B disconnected.
	TCCR1B  = (1 << WGM12);              // CTC
	TCCR1B |= (1 << CS10) | (1 << CS12); // /1024 Clock Select Bit Description
	OCR1A   = F_CPU / 1024 * 2 - 1;      // 0.5Hz
	TIMSK1  = (1 << OCIE1A);             // Timer1 Output Compare Match A Interrupt Enable

	SREG |= (1 << SREG_I);

	for (;;);
}

__attribute__((signal)) void TIMER1_COMPA_vect() {
	uart_printstr(str);
}