#include <avr/io.h>

#define UART_BAUDRATE 115200

// mode 0 / RX
// mode 1 / TX auto
// mode 2 / TX potar

#define BAUD_PRESCALE ((F_CPU / 8 / UART_BAUDRATE) - 1) // Async double speed   20.3.1

volatile uint8_t mode = 0;
volatile uint8_t wait_flag = 0;
uint8_t is_press = 0;
uint8_t tx_start = 0;

void ft_delay_ms(unsigned int n){ // don't work with -Os
	unsigned int x;

	while (n--){
		x = 2300;
		while (x--);
	}
}

void uart_init(void){
	UBRR0L = BAUD_PRESCALE;                 // Set baud rate                     20.11.5 UBRRnL
	UBRR0H = BAUD_PRESCALE >> 8;

	UCSR0A = 1 << U2X0;                     // Double speed                      20.11.2 UCSRnA
	UCSR0B = (1 << RXEN0) | (1 << TXEN0);   // Enable receiver and transmitter   20.11.3 UCSRnB
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); // 8-N-1                             20.11.4 UCSRnC
}

void uart_tx(unsigned char data){
	while(!(UCSR0A & (1 << UDRE0)));        // Wait for empty transmit buffer    20.11.2 UCSRnA
	UDR0 = data;                            //Put data into buffer and sends     20.11.1 UDRn
}

uint8_t uart_rx(void){
	while (!(UCSR0A & (1 << RXC0)));        // Wait for data to be received      20.11.2 UCSRnA
	return UDR0;                            // Get data from buffer              20.11.1 UDRn
}

void uart_printstr(const char *str){
	while (*str)
		uart_tx(*str++);
}

void rainbow(uint8_t pos, uint8_t* r, uint8_t* g, uint8_t* b) {
	pos = 255 - pos;
	if (pos < 85) {
		*r = 255 - pos * 3;
		*g = 0;
		*b = pos * 3;
	}
	else if (pos < 170) {
		pos -= 85;
		*r = 0;
		*g = pos * 3;
		*b = 255 - pos * 3;
	}
	else {
		pos -= 170;
		*r = pos * 3;
		*g = 255 - pos * 3;
		*b = 0;
	}
}

int ft_itoa(int value, char* buffer, int bufferLen) {
	int i = bufferLen - 1, tmp;
	char* ptr = buffer, c;

	for (; value && i; --i) {
		tmp = value;
		value /= 10;
		*ptr++ = "9876543210123456789"[9 + tmp - value * 10];
	}

	if ((tmp < 0) && i) {
		*ptr++ = '-';
		--i;
	}
	if (i) *ptr-- = '\0';
	tmp = ptr - buffer;

	while (buffer < ptr) {
		c = *ptr;
		*ptr-- = *buffer;
		*buffer++ = c;
	}

	return tmp;
}

unsigned int potar_read() {
	ADMUX |= (1 << MUX2);     // ADC4 selected
	ADCSRA |= (1 << ADSC);   // start conversion
	while (!(ADCSRA & (1 << ADIF)));   // wait for ADIF conversion complete return
	ADCSRA |= (1 << ADIF);   // clear ADIF when conversion complete by writing 1
	return (ADC); //return calculated ADC value
}

void start_timer_rx() {
	TCCR1A = 0; // Normal port operation, OC1A/OC1B disconnected.
	TCCR1B = 0;   // normal mode using 0xFFFF as TOP : mode 0 (Table 16-4)
	TCNT1  = 0; // reset counter

	TIMSK1 |= (1 << TOIE1); // Timer 1, Overflow Interrupt Enable ( 16.11.8 )
	SREG |= (1 << SREG_I); // SEI
	TCCR1B |= (1 << CS11);     // /8 Clock Select Bit Description (Table 16-5)
}

void start_timer_tx() {
	TCCR1A = 0; // Normal port operation, OC1A/OC1B disconnected.
	TCCR1A |= (1 << WGM11);
	TCCR1B = (1<<WGM13) | (1<<WGM12); // PWM mode using ICR1 as TOP : mode 14 (Table 16-4)
	TCNT1 = 0; // reset counter

	TIMSK1 |= (1 << TOIE1); // Timer 1, Overflow Interrupt Enable ( 16.11.8 )
	TIMSK1 |= (1 << OCIE1A); // Timer 1, Output Compare A Match Interrupt Enable
	
	SREG |= (1 << SREG_I); // SEI
	TCCR1B |= (1 << CS11); // /8 Clock Select Bit Description (Table 16-5)
	OCR1A = 4000;
	ICR1 = 4000 + 8096 + 8000;
	tx_start = 1;
}

void stop_timer() {
	TCCR1B = 0;
	SREG &= ~(1 << SREG_I); // CLI
}

int main(void){

	uart_init();
	uart_printstr("\r\nStart\r\n");

	DDRB = 0xF; // x4 LEDs OUTPUT
	DDRC = (1 << PIN1); // IR TX output, IR RX input
	DDRD = (1 << PIN3) | (1 << PIN5) | (1 << PIN6); // LED RGB output

	PORTB = (mode) & 0xF;

	// timer 0 PWN color RG
	TCCR0A  = (1 << COM0A1);
	TCCR0A |= (1 << COM0B1);
	TCCR0A |= (1 << WGM21) | (1 << WGM20); // Fast PWM mode 3

	TCCR0B = (1 << CS00); // /1 Clock Select Bit Description(Table 15 - 9)

	OCR0A = 0;
	OCR0B = 0;

	// timer 2 PWM color B
	TCCR2A = (1 << COM2B1);
	TCCR2A |= (1 << WGM21) | (1 << WGM20); // Fast PWM mode 3

	TCCR2B = (1 << CS20); // /1 Clock Select Bit Description(Table 18 - 9)

	OCR2B = 0;


	// timer 1  int RX et TX

	// 16bit  /8
	// 1 tick = 0.5us
	// 1us = 2 tick

	// impulse = HIGH 2000us => HIGH 0-4096us => LOW 4000us
	// impulse en tick = 4000 tick + 0-8096 tick + 8000 tick

	ADMUX = (1 << REFS0); // AVCC with external capacitor at AREF pin (Table 24-3)
	ADCSRA = (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // Enable ADC also set Prescaler as 128 (Table 24-5)

	uint8_t i=0;
	uint8_t r,g,b;
	OCR0A = 255;
	OCR0B = 255;
	OCR2B = 255;
	for (;;){
		if (!(PIND & (1<<PIN2))) {
			if (!is_press) {
				mode++;
				mode%=3;
				PORTB=(mode)&0xF;
				is_press=1;
				switch (mode) {
					case 0:
						uart_printstr("RX mode\r\n");
						tx_start = 0;
						break;
					case 1:
						uart_printstr("TX auto mode\r\n");
						break;
					case 2:
						uart_printstr("TX potar mode\r\n");
						break;
				default:
					break;
				}
				ft_delay_ms(20);
			}
		}
		else {
			is_press = 0;
		}
		switch (mode) {
			case 0:
				{
					wait_flag = 1;
					start_timer_rx();
					while (!(PINC & (1 << PIN5)) && wait_flag); // wait start impu
					if (!wait_flag)
						break;
					start_timer_rx();
					while (PINC & (1 << PIN5) && wait_flag); // wait end impu
					uint16_t time = TCNT1;
					stop_timer();
					if (!wait_flag)
						break;

					time = time-4000; // remove start
					char str[20];
					ft_itoa(time, str, sizeof(str));
					uart_printstr(str);
					uart_printstr("\n\r");
					
					rainbow(time/32, &r, &g, &b);

					OCR0A = 255-r;
					OCR0B = 255-g;
					OCR2B = 255-b;
				}
				break;
			case 1:
				{
					rainbow(i++, &r, &g, &b);
					OCR1A = 4000 + i*32;

					OCR0A = 255-r;
					OCR0B = 255-g;
					OCR2B = 255-b;
					ft_delay_ms(10);
				}
				break;
			case 2:
				{
					i = potar_read() / 4;
					rainbow(i, &r, &g, &b);
					OCR1A = 4000 + i * 32;

					OCR0A = 255 - r;
					OCR0B = 255 - g;
					OCR2B = 255 - b;
					ft_delay_ms(10);
				}
				break;
			default:
				break;
		}
		if (!tx_start && mode != 0) {
			start_timer_tx();
		}
	}
}

__attribute__((signal)) void TIMER1_COMPA_vect() {
	PORTC &= ~(1 << PIN1); // IR TX OFF
}

__attribute__((signal)) void TIMER1_OVF_vect() {
	switch (mode) {
		case 0:
			OCR0A = 255;
			OCR0B = 255;
			OCR2B = 255;
			wait_flag = 0;
			break;
		case 1:
		case 2:
			PORTC |= (1 << PIN1); // IR TX ON
			break;
		
		default:
			break;
	}
}


