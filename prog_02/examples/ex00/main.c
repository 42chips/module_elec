#include <avr/io.h>

int main(void) {
    SREG &= ~(1 << SREG_I);

    DDRB |= (1 << PIN3);      // PB3 OUTPUT

    DDRD &= ~(1 << PIN3);     // PD3 input ( INT1 )
    PORTD |= (1 << PIN3);     // Pull-up

    EICRA |= (1 << ISC10);    // Any logic change on INT1 generates an interrupt request
    EIMSK |= (1 << INT1);     // Turns on INT1

    SREG |= (1 << SREG_I);    // turn on interrupts sei();
    for(;;);
}

__attribute__((signal)) void INT1_vect() {
    PORTB |= (1 << PIN3);
}
