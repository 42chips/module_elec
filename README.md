
## TP

Les rushes en binômes, les autres TP en individuel
On libère les sujets de chaque semaines : Lundi, Mardi puis Mercredi
Puis le rush Samedi-Dimanche

- Programation:
  - prog_0: GPIO  [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/prog_00.fr.pdf), [example](prog_00/examples)
  - prog_1: Timer [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/prog_01.fr.pdf), [example](prog_01/examples)
  - prog_2: INT   [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/prog_02.fr.pdf), [example](prog_02/examples)
  - prog_3: UART  [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/prog_03.fr.pdf), [example](prog_03/examples)
  - prog_rush: Multiplayer (TODO) [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/prog_rush.fr.pdf)
  - ressource:    [prepa](https://gitlab.com/42chips/module_elec/-/blob/master/prog_00/subject/README.md), [schema](https://gitlab.com/42chips/module-elec-pcb-pdip), [video](https://www.youtube.com/watch?v=bzUYar4o2YM&list=PL45wZPWoD9vTDl3GQ8-td_jRp8xq74M4U)
- Tech: 
  - tech_0: THT + EEPROM (TODO) [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/tech_00.fr.pdf)
  - tech_1: SMD + rgb (TODO) [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/tech_01.fr.pdf)
  - tech_2: SMD_avr + anal (TODO) [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/tech_02.fr.pdf)
  - tech_3: debug (TODO) [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/tech_rush.fr.pdf) : [hex](https://gitlab.com/42chips/tp_tech02/-/raw/master/tech_02.hex?inline=false)
  - tech_rush: IR project (TODO) [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/prog_rush.fr.pdf)
  - ressource:     [pcb](https://gitlab.com/42chips/tp_pdip)
- CAO:
  - cao_0: schema [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/cao_00.fr.pdf), [schema](cao_00/subject/tp_smd.pdf), [BOM](cao_00/subject/BOM.csv)
  - cao_1: foot   [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/cao_01.fr.pdf)
  - cao_2: PCB    [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/cao_02.fr.pdf)
  - cao_rush: Gameboy (TODO) [sujet](https://gitlab.com/42chips/module_elec_pdf/-/raw/master/cao_rush.fr.pdf) rush 3 jours
  - ressource:    [pcb](https://gitlab.com/42chips/module_elec_tpsmd)


projet minimum:
  - 1 MCU: famille MegaAVRs ( non megaAVR 0-series )
  - au moins 1 périphérique en plus de l'UART (debug + flash) (i2C, SPI, ADC, ...)
  - pas de lib


example: project smart pot iot
  - mcu: ATmega328PB ( car x2 UART )
  - 1 UART: flash + debug
  - 1 UART: at-cmd vers esp8266 (wifi)
  - ADC: LDR sensor
  - I2C: temp + humidity sensor
  - regu lineaire 3.3V


example: radio tx
  - mcu: ATmega328P
  - 1 UART: flash + debug
  - 1 SPI: nRF24l01+ (2.4Ghz radio)
  - ADC: x4 stick input
  - regu lineaire 5V


example: midi controler
  - mcu: ATmega2560
  - 1 UART: flash + debug
  - 1 UART: midi Out
  - 1 UART: midi In
  - ADC: x16 potentiometer
  - Para: LCD 16x04 
