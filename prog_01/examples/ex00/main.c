#include <avr/io.h>

int main(void)
{
	DDRB   |= (1<<PIN1);                 //output PB1 == OC1A

	TCCR1A  = (1<<COM1A1);               // Clear OC1A on Compare Match, set OC1A at BOTTOM (non-inverting mode) ( Table 16-2 )

	TCCR1A |= (1<<WGM11);
	TCCR1B  = (1<<WGM12) | (1<<WGM13);   // Fast PWM mode using ICR1 as TOP : mode 14 ( Table 16-4 )

	TCCR1B |= (1<<CS12);                 // /256 Clock Select Bit Description ( Table 16-5 )

	ICR1    = F_CPU/256 * 1 - 1;         // 1 seconde
	OCR1A   = F_CPU/256 * .50 - 1 ;      // 50%

	for(;;);
}
