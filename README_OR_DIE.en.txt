* **************************************************************************** *
*                                                                              *
*               README_OR_DIE.txt for 42Born2peer                              *
*               Created on : Mon Oct  5 18:13:59 2015                          *
*               Made by : David "Thor" GIRON <thor@staff.42.fr>                *
*               Updated by : Catherine "Cat" MADINIER <cat@42.fr>              *
*                                                                              *
* **************************************************************************** *


I - Quick start:
----------------

  If you don't care or if you are already familiar with 42Born2Peer:

    1) Install LaTeX on your system

    2) Clone your 42Born2Peer repository

    3) At the root of your 42Born2Peer repository:
       $>git submodule init
       $>git submodule update

    4) $>sudo pip install pygments (admin rights required)
    OR $>pip install --user pygments (no admin rights required)

    5) cd d00/subject && make re

    6) Enjoy
    OR Rage, read below



II - Piscine overview
---------------------

  A Piscine is a collection of educational content designed for the
  discovery of a specific language or technology. This content is
  used over a time span of 14 days in a row, divided as 5 "days" of
  Piscine and 2 days of "rush", twice over.

    - A Piscine "day" is a set of assignments due the next evening,
      from Monday to Friday (hence the 5 days), and so goes the second
      week.

    - A Piscine "rush" refers to a short project available on Friday
      evening and due on Sunday evening. Teams of 2 randomly selected
      students will be created automatically.

  Piscine days and rushes are numbered from index 00 for easier
  reference and are scheduled in a standard and consistent way:

    Activity |     Available     |        Due
    -----------------------------------------------
    D00      | Monday     8:42am | Tuesday   11:42pm
    D01      | Tuesday    8:42am | Wednesday 11:42pm
    D02      | Wednesday  8:42am | Thursday  11:42pm
    D03      | Thursday   8:42am | Friday    11:42pm
    D04      | Friday     8:42am | Saturday  11:42pm
    Rush00   | Friday     7:42pm | Sunday    11:42pm
    D05      | Monday     8:42am | Tuesday   11:42pm
    D06      | Tuesday    8:42am | Wednesday 11:42pm
    D07      | Wednesday  8:42am | Thursday  11:42pm
    D08      | Thursday   8:42am | Friday    11:42pm
    D09      | Friday     8:42am | Saturday  11:42pm
    Rush01   | Friday     7:42pm | Sunday    11:42pm

  Piscine days must be designed in a way that allows students to
  discover the topic gradually, starting from the basics and building
  up from there, one day after the other. As a consequence, Piscine days should
  be as consistent and precisely delimited as possible, focusing on a single
  notion at a time, or on a small set of semantically related notions.

  Rushes should not introduce new notions: they are a practical
  overview of the notions discovered so far.

  Piscine days and rushes are abstractions substantiated by concrete and
  specific material:

    - A PDF subject, written using LaTeX (https://www.latex-project.org/).
      A comprehensive description of what is expected from a subject, a
      template and its full documentation are available in the
      subject/ folder of each day and each rush of this repository.

    - A grading scale, written as a YAML description (http://yaml.org/).
      Again, a comprehensive description of what is expected from a
      scale, a template and its full documentation are available in
      the scale/ folder of each day and each rush of this repository.

    - A set of short educational videos covering the notions of the
      day. Therefore, rushes don't have videos since they don't
      introduce new notions. When you design the videos, you must find a
      balance between explaining enough so the student can start working on
      the first assignments of the day, and inciting the student to discover
      things by himself/herself.

  Writing and recording videos is the most time consuming part of the
  creation of a Piscine. That's why every code sample used must be
  written prior to the shooting to ensure that the style is good
  enough and to avoid live misspells and compiling errors.

  42 provides a video recording studio along with its technical
  staff. For each shooting session, a member of the Pedago staff will
  be present to supervise the session and help the author to produce
  the best possible result.



III - Overview of your repository
---------------------------------

  A default and clean Piscine repository for 42Born2Peer consists in a
  list of folders: one per day and one per rush, a common-tex/ folder
  used to centralize general Piscine instructions included in each
  subject, and a submodule resources/. This submodule is
  git@born2peer.42.fr:42born2peer/resources.git, it provides resources
  and style classes for LaTeX.

  Each day or rush folder is organized as follows:

  - correction/
    Sources of your fully functional implementation of the
    assignments.

  - scale/
    This is where you will write your grading scale as a YAML
    description. The d**.*.yml files are self documented templates for
    your grading scale in different languages. Please git delete the
    template files you don't need.

  - subject/
    Obviously, this is where you will write your subject. Likewise,
    you will find self documented LaTeX templates for you subject.
    Delete the templates for the languages you don't need. Please note
    that the provided file 42.png is only used in the templates to
    illustrate how to add pictures in your pdf. Feel free to git
    delete it.

  - videos/
    This folder will contain all code samples that you will use in the
    videos coupled to this Piscine day. You must use a simple and
    clean coding style.



IV - Setup
----------

  In order to write your subject, you need a functional LaTeX
  distribtion installed on your system. We recommand the following:

  - Linux: Texlive (via your distribution packages manager)
  - OSX: MacTex (http://tug.org/cgi-bin/mactex-download/MacTeX.pkg)

  It might be possible to work under Windows using MiKTeX, but I've
  never tested it myself and I don't know anybody who did. As a
  consequence, if you choose to work on Windows, I can't guarantee
  anything and I can't give you any support. Please, seriously
  consider switching to Linux or OSX if you experience troubles.

  For 42 students, MacTex is available for installation from the MSC
  on your session. Everything is downloaded and installed for you,
  quick and painless.

  IMPORTANT: After installation, you might need to configure your
  $PATH variable in order to allow your system to find LaTeX
  binaries. For instance, the MacTex package for OSX usually installs
  your LaTeX distribution in /usr/texbin/ (this might be sligthly
  different from one version to the other). This folder is not part of the
  usual PATH, so you must add it to your $PATH shell variable
  yourself. This behaviour has also been witnessed under some Linux
  distributions and is resolved the same way. A reliable symptom of
  this problem is a 'command not found' for "pdflatex" when trying to
  build a pdf after a succesful installation of LaTeX.

  As introduced in the previous section, the resources/ folder is a
  submodule provided by 42's Pedago team. Its content will allow you
  to create pdfs that look like genuine 42 subjects. You will NEVER
  have to change anything in those files. Never. Don't even bother
  reading them if you are not interested. The self documented templates
  for your subject provided in the subject/ folder are already
  configured to use our LaTeX resources and style classes, no
  configuration of LaTeX or additional code is expected from you,
  as long as a clean and functional LaTeX distribution is installed
  on your system.

  However, the resources/ GiT submodule must be initialized before the
  first use. If you never used GiT submodules, please skim through
  this documentation to get a grasp of the concept:
  https://git-scm.com/book/en/v2/Git-Tools-Submodules. When you're
  done with your reading, you will probably understand that you need
  to type these four commands at the root of your repository:

    $>git submodule init
    $>git submodule update
    $>cd resources/
    $>git pull origin master

  Next step, you must install the "pygments" Python package as our
  LaTeX styles classes relies on this package to provide syntactic
  highlights in the generated pdfs. Choose your favorite Python
  package manager, like "easy_install" or "pip". For instance using
  "pip":

    $>sudo pip install pygments

  If you work as a user on your session and don't have admin rights,
  use the following command instead:

    $>pip install --user pygments

  Of course, installing Pygments is needed only once. If you create
  several 42Born2Peer projects or Piscines, you can skip this step
  from the second time onwards.

  Last step, you must install the Kwalify Ruby paquet to use the scale
  validator script.

    $>gem install kwalify

   If you work as a user on your session and don't have admin rights,
  use the following command instead:

    $>gem install --user kwalify

  Just like Pygments, you only have to install it once.



V - Support
-----------

  You can contact us at pedago@42.fr if you need help regarding the
  writing of your educational content.


* **************************************************************************** *
