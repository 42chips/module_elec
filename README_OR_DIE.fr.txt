* **************************************************************************** *
*                                                                              *
*               README_OR_DIE.txt for 42Born2peer                              *
*               Created on : Mon Oct  5 18:13:59 2015                          *
*               Made by : David "Thor" GIRON <thor@staff.42.fr>                *
*               Updated by : Catherine "Cat" MADINIER <cat@42.fr>              *
*                                                                              *
* **************************************************************************** *


I - Démarrage rapide
--------------------

  Si vous êtes préssé ou déjà familier avec 42Born2Peer :

    1) Installez LaTeX sur votre système

    2) Clonez votre dépôt 42Born2Peer

    3) A la racine de votre dépôt 42Born2Peer :
       $>git submodule init
       $>git submodule update

    4) $>sudo pip install pygments (droits administrateur requis)
    OU $>pip install --user pygments (pas de droits administrateur requis)

    5) cd d00/subject && make re

    6) Profitez
    OU Ragez, lisez ce qui suit



II - Vue d'ensemble d'une Piscine
---------------------------------

  Une Piscine est un ensemble de contenus pédagogiques destinés à
  découvrir un langage ou une technologie particulière. Ces contenus
  fonctionnent sur une période de 14 jours consécutifs divisés en 5
  "jours" de Piscine et 2 jours de "rush", deux fois de suite.

    - Un "jour" de Piscine est un ensemble d'exercices à rendre le
      lendemain soir, du lundi au vendredi (d'où les 5 jours). Il en va
      de même pour la deuxième semaine.

    - Un "rush" de Piscine est un projet court disponible le vendredi
      soir et à rendre le dimanche soir. Des équipes de 2 étudiants
      seront créées automatiquement.

  Le jours de Piscine et les rushes sont numérotés depuis l'index 00
  pour s'y référer facilement et sont planifiés de façon standard et
  cohérente :

    Activité |   Disponible   |   A rendre
    -----------------------------------------
    D00      | Lundi     8h42 | Mardi    23h42
    D01      | Mardi     8h42 | Mercredi 23h42
    D02      | Mercredi  8h42 | Jeudi    23h42
    D03      | Jeudi     8h42 | Vendredi 23h42
    D04      | Vendredi  8h42 | Samedi   23h42
    Rush00   | Vendredi 19h42 | Dimanche 23h42
    D05      | Lundi     8h42 | Mardi    23h42
    D06      | Mardi     8h42 | Mercredi 23h42
    D07      | Mercredi  8h42 | Jeudi    23h42
    D08      | Jeudi     8h42 | Vendredi 23h42
    D09      | Vendredi  8h42 | Samedi   23h42
    Rush01   | Vendredi 19h42 | Dimanche 23h42

  Les jours de Piscine doivent être conçus de façon à permettre à un
  étudiant de découvrir un thème de manière incrémentale, en
  commençant par les bases, puis d'avancer, un jour après
  l'autre. Cela implique donc que les jours de piscine devraient être
  aussi précisément délimités que possible, en se concentrant sur une
  seule notion à la fois, ou bien sur un ensemble restreint de
  notions sémantiquement liées.

  Les rushes ne devraient pas introduire de nouvelles notions : ce
  sont des résumés pratiques des notions découvertes jusque là.

  Les jours de Piscine et les rushs sont des abstractions à éléments
  tangibles et spécifiques :

    - Un document PDF de sujet, rédigé en LaTeX
      (https://www.latex-project.org/). Une description exhaustive de
      ce qui est attendu d'un sujet, un template et sa documentation
      complète sont à votre disposition dans le dossier subject/ de
      chaque jour et de chaque rush de ce dépôt.

    - Un barème de notation, rédigé sous la forme d'une description
      YAML (http://yaml.org/). De même, une description exhaustive de
      ce qui est attendu d'un barème, un template et sa documentation
      complète sont à votre disposition dans le dossier scale/ de
      chaque jour et de chaque rush de ce dépôt.

    - Un ensemble de courtes vidéos pédagogiques couvrant les notions
      de la journée. Par conséquent, les rushes n'ont pas de vidéos
      puisqu'ils n'introduisent pas de nouvelles notions. Les vidéos
      doivent trouver un équilibre entre expliquer suffisamment pour
      permettre à l'étudiant(e) de commencer à travailler sur les
      premiers exercices de la journée, et pousser l'étudiant(e) à
      découvrir des choses par lui/elle-même.

  Ecrire et enregistrer les vidéos est la partie la plus chronophage
  de la création d'une Piscine. C'est pourquoi chaque exemple de code
  utilisé doit être rédigé avant l'enregistrement pour nous assurer
  que le style est suffisamment bon et pour éviter les erreurs de
  syntaxe ou de compilation durant la performance.

  42 met à votre disposition un studio d'enregistrement vidéo, ainsi
  que son personnel technique. Pour chaque scéance d'enregistrement,
  un membre de l'équipe Pedago sera présent pour superviser la séance
  et aider l'auteur à obtenir la meilleure performance possible.



III - Vue d'ensemble de votre dépôt
-----------------------------------

  Un dépôt 42Born2Peer de piscine avec ses paramètres par défaut est
  composé d'une liste de dossiers : un par journée de piscine et un par
  rush, un dossier common-tex/ servant à centraliser les consignes
  valables pour toute la Piscine et inclues dans chaque sujet, et un
  submodule resources/. Ce submodule est
  git@born2peer.42.fr:42born2peer/resources.git, il fournit les
  ressources et les classes de style pour LaTeX.

  Chaque dossier de journée ou de rush est organisé de la façon
  suivante :

  - correction/
    Les sources de votre implémentation fonctionnelle des exercices.

  - scale/
    C'est là que vous rédigerez votre barème de notation sous la forme d'une
    description YAML. Les fichiers d**.*.yml sont des templates
    auto-documentés pour votre barème de notation, dans différentes
    langues. Prenez soin de git delete les fichiers template dont vous
    n'avez pas besoin.

  - subject/
    Sans surprise, c'est là que vous rédigerez votre sujet. De la même
    façon, vous trouverez des templates auto-documentés pour votre
    sujet. Effacez les templates dans les langues dont vous n'aurez
    pas besoin. Pour information, le fichier 42.png n'est utilisé que
    dans ces templates pour illustrer comment inclure des images dans
    votre PDF. N'hésitez pas à l'effacer.

  - videos/
    Ce dossier devra accueillir tous les exemples de code que vous
    utiliserez dans les vidéos associées à cette journée. Vous devez
    utiliser un style de code clair et propre.



IV - Installation
-----------------

  Afin de pouvoir rédiger votre sujet, vous devez avoir une
  installation LaTeX fonctionnelle sur votre système. Nous
  recommandons les distributions suivantes :

  - Linux: Texlive (via votre manager de paquets)
  - OSX: MacTex (http://tug.org/cgi-bin/mactex-download/MacTeX.pkg)

  Il devrait être possible de travailler sous Windows avec MiKTeX,
  mais je ne l'ai jamais essayé moi-même et je ne connais personne qui
  l'ait fait. Donc si vous choisissez de travailler sous Windows, je
  ne peux rien garantir et je ne pourrai pas vous apporter
  d'aide. Veuillez s'il vous plait envisager de passer sous Linux ou
  sous OSX si vous rencontrez des problèmes.

  Pour les étudiants de 42, MacTex peut être installé via le MSC
  de votre session. Tout est téléchargé et installé pour vous,
  rapidement et sans douleur.

  IMPORTANT : Après l'installation, vous pourriez avoir besoin de
  configurer votre variable $PATH afin de permettre à votre système de
  trouver les binaires LaTeX. Par exemple, le paquet MacTex pour OSX
  installe généralement votre distribution LaTeX dans /usr/texbin
  (ceci peut varier légèrement d'une version à l'autre). Ce dossier ne
  fait pas parti du PATH usuel, vous devez donc l'ajouter à votre
  variable d'environnement $PATH vous-même. Ce comportement a
  également été observé pour certaines distributions Linux et est
  résolu de la même façon. Un symptôme fiable de ce problème est un
  'command not found' pour "pdflatex" quand vous tentez de contruire un
  PDF avec une installation de LaTeX pourtant correcte.

  Comme présenté dans la section précédente, le dossier resources/ est
  un submodule fourni par l'équipe Pédago. Son contenu vous permet de
  créer des PDF qui ont l'aspect visuel officiel des sujets de
  42. Vous n'aurez JAMAIS besoin de changer quoi que ce soit dans ces
  fichiers. Jamais. Ne vous embêtez même pas à les lire si cela ne
  vous intéresse pas. Les templates auto-documentés pour votre sujet
  sont déjà configurés pour utiliser nos ressources LaTeX et nos
  classes de style. Aucune configuration supplémentaire de LaTeX ou
  code additionnel ne sont nécéssaires de votre part si vous avez
  bien une installation propre et fonctionnelle de LaTeX sur votre
  système.

  Toutefois, avant toute utilisation, le submodule GiT resources/ doit
  être initialisé. Si vous n'avez jamais utilisé de submodules GiT,
  prenez le temps de parcourir cette documentation pour découvrir le
  sujet : https://git-scm.com/book/en/v2/Git-Tools-Submodules. Après
  votre lecture, vous devriez probablement avoir compris que vous
  devez taper ces quatre commandes à la racine de votre dépôt :

    $>git submodule init
    $>git submodule update
    $>cd resources/
    $>git pull origin master

  Etape suivante, vous devez installer le paquet Python "pygments"
  utilisé par nos classes de style LaTeX qui en dépendent pour vous
  fournir de la coloration syntaxique dans les PDFs générés.
  Choisissez votre gestionnaire de paquet Python favori, comme
  "easy_install" ou "pip". Par exemple en utilisant "pip" :

    $>sudo pip install pygments

  Si vous travaillez en tant qu'utilisateur sans droits admin
  sur votre session, utilisez plutôt la commande :

    $>pip install --user pygments

  Bien entendu, il n'est nécessaire d'installer Pygments qu'une seule
  fois. Si vous créez plusieurs projets ou Piscines 42Born2Peer, vous
  pouvez sauter cette étape la seconde fois.

  Dernière étape, vous devez installer le paquet Ruby Kwalify pour
  utiliser le script de validation de barèmes.

    $>gem install kwalify

  Si vous travaillez en tant qu'utilisateur sans droits admin
  sur votre session, utilisez plutôt la commande :

    $>gem install --user kwalify

  Comme pour Pygments, vous n'avez à installer ce paquet qu'une seule
  fois.



V - Support
-----------

  Vous pouvez nous contacter à l'adresse pedago@42.fr si vous avez
  besoin d'aide pour la rédaction de vos contenus pédagogiques.


* **************************************************************************** *
